# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import sys as sstm
from pathlib import Path as path_t

from brick.feature.analysis import Main as Analysis
# from brick.io.storage.parameters import NutrimorphArguments

_CSV_FEATURE_FILE = "/home/eric/Code/project/mno/nutrimorph/2-morgane/_result-SKIP/complete-20W/hypothalamus-priority/features.csv"

def Main():
    """"""
    if sstm.argv.__len__() < 2:
        print("Missing parameter file argument")
        sstm.exit(0)
    argument = path_t(sstm.argv[1])
    if not (argument.is_file() and (argument.suffix.lower() == ".ini")):
        print("Wrong parameter file path or type, or parameter file unreadable")
        sstm.exit(0)

    # config = NutrimorphArguments(argument)
    # (
    #     *_,
    #     input_cfg,
    #     output_cfg,
    #     __,
    #     ___,
    # ) = config
    # base_folder = sorted((output_cfg["save_images"] / input_cfg["data_path"].stem).glob("2023*"))[-1]
    # output_cfg["save_images"] = base_folder / "image"
    # output_cfg["save_csv"] = base_folder / "features"
    # output_cfg["save_features_analysis"] = base_folder / "analysis"

    # path_1 = output_cfg["save_csv"]
    # path_2 = output_cfg["save_features_analysis"]
    # print(base_folder, path_1, path_2, sep="\n")
    Analysis(_CSV_FEATURE_FILE, path_t(_CSV_FEATURE_FILE).parent / "analysis")#str(path_1 / _CSV_FEATURE_FILE), path_2)


if __name__ == "__main__":
    #
    Main()
