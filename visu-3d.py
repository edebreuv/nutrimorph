import numpy as nmpy
from skimage import filters as fltr

import brick.io.screen.type.soma_validation as smvl


def Main() -> None:
    """"""
    image = nmpy.zeros((100, 200, 300), dtype=nmpy.float64)
    row_grid, col_grid, dep_grid = nmpy.meshgrid(
        range(image.shape[0]),
        range(image.shape[1]),
        range(image.shape[2]),
        indexing="ij",
    )
    for label, (center, radii) in enumerate(
        zip(((50, 40, 60), (30, 80, 220)), ((40, 20, 20), (30, 30, 60))), start=1
    ):
        image[
            ((row_grid - center[0]) / radii[0]) ** 2
            + ((col_grid - center[1]) / radii[1]) ** 2
            + ((dep_grid - center[2]) / radii[2]) ** 2
            <= 1.0
        ] = label

    smoothed = fltr.gaussian(image, 1.0)
    labeled = image.astype(nmpy.uint8)

    window = smvl.soma_validation_window_t(
        smoothed, labeled, mip_axis=0, gray_offset=40
    )
    n_somas = window.LaunchValidation()
    print(f"Validated Somas: {n_somas}")


if __name__ == "__main__":
    #
    Main()
