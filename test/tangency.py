# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as nmpy


def Test_Tangency_ext_conn(soma):
    """"""
    pb = []
    for extension in soma.extensions:
        for site in list(zip(*extension.sites)):
            close_end_pt = tuple(
                (site[0] + i, site[1] + j, site[2] + k)
                for i in (-1, 0, 1)
                for j in (-1, 0, 1)
                for k in (-1, 0, 1)
                if i != 0 or j != 0 or k != 0
            )
            for ext in tuple(zip(soma.connection_path.values())):
                if ext[0] is not None:
                    for conn in list(zip(*nmpy.asarray(ext[0]))):
                        if (conn in close_end_pt) and (
                            conn not in list(zip(*extension.sites))
                        ):
                            for extens in soma.extensions:
                                if conn not in zip(*extens.end_points):
                                    if conn not in pb:
                                        pb.append(conn)
                                        print(
                                            "\nconn: ",
                                            conn,
                                            "\next_site: ",
                                            site,
                                            "\next_uid: ",
                                            extension.uid,
                                        )
