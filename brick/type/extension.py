# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

import dataclasses as dtcl
from typing import Any, Sequence, Tuple

import frangi3 as fg_
import numpy as nmpy
import skimage.morphology as mp_
from logger_36 import LOGGER
from scipy import ndimage as im_
from skl_graph.type.skl_map import SKLMapFromObjectMap
from skl_graph.type.tpy_map import TopologyMapOfMap

from brick.task.img_processing import (
    AbsoluteHysteresisThresholded,
    InPlaceMorphologicalCleaning,
)
from brick.type.base import array_t, site_h
from brick.type.component import component_t


@dtcl.dataclass(slots=True, repr=False, eq=False)
class extension_t(component_t):
    end_points: tuple[array_t, ...] = None
    frangi_scales: array_t = None
    soma_uid: int = None  # Connected to a soma somewhere upstream
    __cache__: dict[str, Any] = dtcl.field(default_factory=dict)

    @classmethod
    def NewFromMap(
        cls, lmp: array_t, frangi_scales: array_t, uid: int, /
    ) -> extension_t:
        """"""
        bmp = lmp == uid

        instance = cls(end_points=cls.EndPointMap(bmp).nonzero())
        instance.InitializeFromMap(bmp, uid)
        instance.frangi_scales = frangi_scales[instance.sites]

        return instance

    @property
    def is_unconnected(self) -> bool:
        """"""
        return self.soma_uid is None

    @property
    def end_points_as_array(self) -> array_t:
        """"""
        pty_name = "end_points_as_array"
        if pty_name not in self.__cache__:
            self.__cache__[pty_name] = nmpy.array(self.end_points)

        return self.__cache__[pty_name]

    def EndPointsForSomaOrExtension(
        self, soma_uid: int, influence_map: array_t, /
    ) -> tuple[site_h, ...]:
        """
        Find the extensions endpoints in the influence of the soma
        """
        ep_bmp = influence_map[self.end_points] == soma_uid  # bmp=boolean map

        if nmpy.any(ep_bmp):
            end_point_idc = ep_bmp.nonzero()[0]
            end_points = self.end_points_as_array[:, end_point_idc]

            # Return coordinates of endpoints under a tuple[(x1,y1,z1),(x2,y2,z2),...] format
            return tuple(zip(*end_points.tolist()))

        return ()

    def BackReferenceSoma(self, glial_cmp: component_t, /) -> None:
        """"""
        if isinstance(glial_cmp, extension_t):
            self.soma_uid = glial_cmp.soma_uid
        else:
            self.soma_uid = glial_cmp.uid

    def __str__(self) -> str:
        """"""
        if self.extensions is None:
            n_extensions = 0
        else:
            n_extensions = self.extensions.__len__()

        return (
            f"Ext.{self.uid}, "
            f"sites={self.sites[0].__len__()}, "
            f"endpoints={self.end_points[0].__len__()}, "
            f"soma={self.soma_uid}, "
            f"extensions={n_extensions}"
        )

    @staticmethod
    def ExtensionContainingSite(
        extensions: Sequence[extension_t], site: site_h, /
    ) -> extension_t | None:
        """"""
        for extension in extensions:
            if site in tuple(zip(*extension.sites)):
                return extension

        return None

    @staticmethod
    def EnhancedForDetection(
        image: array_t,
        scale_range,
        scale_step,
        alpha,
        beta,
        frangi_c,
        bright_on_dark,
        method,
        diff_mode,
        /,
        *,
        in_parallel: bool = False,
    ) -> Tuple[array_t, array_t]:
        """
        Preprocess by white top hat, then perform Frangi vesselness enhancement
        """
        preprocessed_img = im_.morphology.white_tophat(
            image, size=2, mode="constant", cval=0.0, origin=0
        )

        enhanced_img, scale_map = fg_.FrangiEnhancement(
            preprocessed_img,
            scale_range,
            scale_step=scale_step,
            alpha=alpha,
            beta=beta,
            frangi_c=frangi_c,
            bright_on_dark=bright_on_dark,
            in_parallel=in_parallel,
            method=method,
            differentiation_mode=diff_mode,
        )

        return enhanced_img, scale_map

    @staticmethod
    def CoarseMap(
        image: array_t, low: float, high: float, selem: array_t, min_area: int, /
    ) -> array_t:
        """"""
        LOGGER.info("    Hysteresis thresholding...")
        output = AbsoluteHysteresisThresholded(image, low, high)

        LOGGER.info("    Morphological cleaning...")
        InPlaceMorphologicalCleaning(output, selem)

        LOGGER.info("    Small object removal...")
        assert nmpy.issubdtype(output.dtype, nmpy.bool_)
        output = mp_.remove_small_objects(
            output,
            min_size=min_area,
            connectivity=output.ndim,
        )

        return output

    @staticmethod
    def FineMapFromCoarseMap(
        coarse_map: array_t, /, *, soma: array_t = None
    ) -> array_t:
        """"""
        output = SKLMapFromObjectMap(coarse_map)

        if soma is not None:
            output[soma] = False

        return output

    @staticmethod
    def EndPointMap(map_: array_t, /) -> array_t:
        """"""
        return TopologyMapOfMap(map_) == 1
