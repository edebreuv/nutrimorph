# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

import dataclasses as dtcl
import sys as sy_
from typing import Sequence

import numpy as nmpy
import scipy.ndimage as im_
import skimage.measure as ms_
import skimage.morphology as mp_
from logger_36 import LOGGER
from skimage.segmentation import relabel_sequential as RelabeledSequentially
from skl_graph.type.tpy_map import TopologyMapOfMap

from brick.task.img_processing import (
    GapHysteresisThresholded,
    InPlaceMorphologicalCleaning,
)
from brick.type.base import array_t, py_array_picker_h, site_h
from brick.type.component import component_t
from brick.type.graph import skl_graph_t


@dtcl.dataclass(slots=True, repr=False, eq=False)
class soma_t(component_t):
    contour_points: tuple[tuple[int, int, int], ...] = None
    centroid: array_t = None
    skl_graph: skl_graph_t = None
    ext_roots: list = dtcl.field(default_factory=list)
    graph_roots: dict = None
    volume_soma_micron: float = None
    axes_ellipsoid: float = None

    @classmethod
    def NewFromMap(cls, lmp: array_t, uid: int, /) -> soma_t:
        """"""
        bmp = lmp == uid
        contour_map = cls.ContourMap(bmp)

        instance = cls(contour_points=tuple(zip(*contour_map.nonzero())))
        instance.InitializeFromMap(bmp, uid)
        instance.centroid = nmpy.array(
            [nmpy.median(instance.sites[i]) for i in range(3)]
        )

        return instance

    def Extensions(self, /, *, max_level: int = sy_.maxsize) -> tuple[component_t]:
        """
        max_level=1: directly connected extensions
        """
        soma_ext = self.extensions.copy()

        current_level_idx = 0
        for level in range(2, max_level + 1):
            next_level_idx = soma_ext.__len__()
            if next_level_idx == current_level_idx:
                break

            for extension in soma_ext[current_level_idx:]:
                soma_ext.extend(extension.extensions)

            current_level_idx = next_level_idx

        return tuple(soma_ext)

    def ContourPointsCloseTo(
        self, point: site_h, max_distance: float, /
    ) -> tuple[tuple[site_h, ...] | None, py_array_picker_h | None]:
        """
        Find the closest contour points of soma to an extension endpoint in the 3D map:
        Find the points in the contour points of the soma which euclidian distance to the endpoint is inferior to the
        maximum distance allowed.
        """
        points = tuple(
            contour_point
            for contour_point in self.contour_points
            if (nmpy.subtract(point, contour_point) ** 2).sum() <= max_distance
        )

        return __PointsCloseTo__(points)

    def ExtensionPointsCloseTo(
        self, point: site_h, max_distance: float, /
    ) -> tuple[tuple[site_h, ...] | None, py_array_picker_h | None]:
        """
        Find the closest extensions point to an extension endpoint in the 3D map.
        Leave connection paths apart because only detected extension pieces
        (as opposed to invented=connection paths) are considered valid connection targets.
        """
        points = []

        # Find the points in the extensions sites which euclidian distance to the endpoint is inferior to the
        # maximum distance allowed.
        for extension in self.Extensions():
            ext_sites = tuple(zip(*extension.sites))
            ext_sites = (
                ext_point
                for ext_point in ext_sites
                if (nmpy.subtract(point, ext_point) ** 2).sum() <= max_distance
            )
            # TODO: points already ordered
            # filtrer si endpoint unique on le garde, sinon on enleve les points trop pres du bord
            # (2 voxels = soustraire les point 0,1 et -1,-2)
            points.extend(ext_sites)

        return __PointsCloseTo__(tuple(points))

    def BackReferenceSoma(self, glial_cmp: component_t, /) -> None:
        """"""
        raise NotImplementedError("Soma do not need to back-reference a soma")

    def __str__(self) -> str:
        """"""
        if self.extensions is None:
            n_extensions = 0
        else:
            n_extensions = self.extensions.__len__()

        return (
            f"Soma.{self.uid}, "
            f"area={self.sites[0].__len__()}, "
            f"contour points={self.contour_points.__len__()}, "
            f"extensions={n_extensions}"
        )

    @staticmethod
    def Maps(
        image: array_t,
        low: float,
        high: float,
        selem: array_t,
        min_area: int,
        max_area: int,
        /,
    ) -> tuple[array_t, array_t, array_t, int]:
        """"""
        LOGGER.info("    Hysteresis thresholding...")
        bln_map = GapHysteresisThresholded(image, low, high)

        LOGGER.info("    Morphological cleaning...")
        InPlaceMorphologicalCleaning(bln_map, selem)

        LOGGER.info("    Small object removal...")
        assert nmpy.issubdtype(bln_map.dtype, nmpy.bool_)
        bln_map = mp_.remove_small_objects(
            bln_map,
            min_size=min_area,
            connectivity=bln_map.ndim,
        )

        bln_map_w_large = bln_map.copy()

        lbl_map = ms_.label(bln_map)

        LOGGER.info("    Large object removal...")
        for region in ms_.regionprops(lbl_map):
            if region.area >= max_area:
                where = lbl_map == region.label
                bln_map[where] = False
                lbl_map[where] = 0
        lbl_map, *_ = RelabeledSequentially(lbl_map)
        n_somas = nmpy.amax(lbl_map).item()
        assert nmpy.array_equal(
            nmpy.array(tuple(range(n_somas + 1))), nmpy.unique(lbl_map)
        )

        return bln_map, lbl_map, bln_map_w_large, n_somas

    @staticmethod
    def ContourMap(map_: array_t, /) -> array_t:
        """"""
        return TopologyMapOfMap(map_) < 26

    @staticmethod
    def InfluenceMaps(map_: array_t, /) -> tuple[array_t, array_t]:
        """"""
        dist_map, idx_map = im_.morphology.distance_transform_edt(
            map_ == 0, return_indices=True
        )

        return dist_map, nmpy.array(map_[tuple(idx_map)])

    @staticmethod
    def SomasLMap(
        somas: Sequence[soma_t], /, *, with_extensions: bool = True
    ) -> array_t:
        """
        Create a map containing the labelled somas and their extensions and connection paths
        with the label of their respective soma.
        """
        if somas.__len__() == 0:
            raise AssertionError("No soma detected on the image.")

        shape = somas[0].img_shape
        result = nmpy.zeros(shape, dtype=nmpy.int64)

        # for each soma, put into the map the soma label at the sites of soma, connexion paths and extensions.
        for soma in somas:
            result[soma.sites] = soma.uid

            # if the soma has extensions, add the soma label at the site of the extensions and of the connection path
            if with_extensions:
                # For each existing connexion path soma-ext, add it on the result map with soma label
                for connection_path in filter(
                    lambda path: path is not None, soma.connection_path.values()
                ):
                    result[connection_path] = soma.uid

                # For each existing connexion path ext-ext, add it on the result map with soma label
                for extension in soma.Extensions():
                    for connection_path in filter(
                        lambda path: (path is not None) and (path is not {}),
                        extension.connection_path.values(),
                    ):
                        result[connection_path] = soma.uid

                    # Also add the extensions on the result map with soma label
                    result[extension.sites] = soma.uid

        return result


def __PointsCloseTo__(
    points: tuple[site_h, ...], /
) -> tuple[tuple[site_h, ...] | None, py_array_picker_h | None]:
    """
    Reformat the 3D points coordinates
    """
    if points.__len__() > 0:
        points_as_picker = tuple(zip(*points))
    else:
        points = None
        points_as_picker = None

    return points, points_as_picker
