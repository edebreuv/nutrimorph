# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as nmpy
import skimage.measure as ms_
import sklearn.decomposition as dc_


def Soma_parameters(image, final_soma_label):
    #
    (w, n, m) = image.shape
    soma_label = final_soma_label
    soma_p1 = []
    soma_p2 = []
    soma_p3 = []
    soma_p4 = []
    soma_p5 = []
    area = ms_.regionprops(soma_label)

    # area paramter
    for region in area:
        soma_p1.append(region.centroid)
        soma_p2.append(region.area)
        soma_p3.append(region.coords)
        soma_p4.append(region.moments)

    # compute eccentricity using PCA
    pca = dc_.PCA()
    for l in nmpy.unique(soma_label):
        if l != 0:
            region = nmpy.zeros([w, n, m])
            loc = nmpy.where(soma_label == l)
            region[loc] = image[loc]
            loc = nmpy.asarray(loc)
            # Estimation, calcul des composantes principales for each soma
            C = pca.fit(loc).transform(loc)
            egv1 = (C[0][:]).max()
            egv2 = (C[1][:]).max()
            egv3 = (C[2][:]).max()
            egv_vect = nmpy.array([egv1, egv2, egv3])
            egv_max = nmpy.amax(egv_vect)
            ration1 = egv1 / egv_max
            ration2 = egv2 / egv_max
            ration3 = egv3 / egv_max

            loc = nmpy.where(egv_vect == egv_max)
            vect_ration = nmpy.array([ration1, ration2, ration3])
            ration = nmpy.delete(vect_ration, loc)
            soma_p5.append(ration)
    soma_p1 = nmpy.asarray(soma_p1)
    soma_p2 = nmpy.asarray(soma_p2)
    soma_p3 = nmpy.asarray(soma_p3)
    soma_p4 = nmpy.asarray(soma_p4)
    soma_p5 = nmpy.asarray(soma_p5)

    parameters_soma = nmpy.array(
        [soma_p1, soma_p2, soma_p3, soma_p4, soma_p5[:, 0], soma_p5[:, 1]]
    )
    parameters_soma = parameters_soma.T

    return parameters_soma
