# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as nmpy

from brick.type.base import array_t


def ImageVerification(image: array_t, channel: str, /) -> array_t:
    """
    Verification of the dimension of the image and of the coherence with the entered 'channel' parameter.
    """
    if nmpy.amax(image) == nmpy.amin(image):
        raise ValueError("The input image should not be constant.")

    # Verification of the dimension of the image and its coherence with the parameters (channel)
    elif image.ndim == 3:
        if channel is not None:
            print(
                "Warning: The image has only 3 dimensions. Give the channel the value None in the parameters."
            )
        return image

    elif image.ndim == 4:
        if channel == "R" or channel == "G" or channel == "B":
            # not changed into if --channel in 'RGB'-- because if channel='RG' => True.
            print("MULTIPLE CHANNELS: ", channel, " specified in the parameters.")

            image = image[:, :, :, "RGB".find(channel)]

            # The obtained image must not be constant
            if image.max() == image.min():
                raise ValueError(
                    "The input image should not be constant. Verify the channel parameter."
                )

            return image

        else:
            raise ValueError(
                "The image has multiple color channels. Error in the value of the parameter channel."
            )

    elif image.ndim != 4 and image.ndim != 3:
        raise ValueError(
            f"The image dimensions are not correct: {image.ndim}, instead of 3 or 4."
        )
