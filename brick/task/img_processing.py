# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as nmpy
import skimage.filters as fl_
import skimage.morphology as mp_

from brick.type.base import array_t


def IntensityNormalizedImage(image: array_t, /) -> array_t:
    """
    Relative normalization of the image between 0 and 1.
    No division per 0 since the image should not be constant (ImageVerification function).
    """
    value_max = nmpy.amax(image).item()
    value_min = nmpy.amin(image).item()

    return (image.astype(nmpy.float32) - value_min) / float(value_max - value_min)


def GapHysteresisThresholded(image: array_t, low: float, high: float) -> array_t:
    """"""
    max_intensity = nmpy.amax(image).item()
    min_non_zero = nmpy.amin(image[image > 0]).item()
    gap = max_intensity - min_non_zero

    low = low * gap + min_non_zero
    high = high * gap + min_non_zero

    return fl_.apply_hysteresis_threshold(image, low, high)


def AbsoluteHysteresisThresholded(image: array_t, low: float, high: float) -> array_t:
    """"""
    low = low * nmpy.amin(image[image > 0]).item()
    high = high * nmpy.amax(image).item()

    return fl_.apply_hysteresis_threshold(image, low, high)


def InPlaceMorphologicalCleaning(image: array_t, selem: array_t, /) -> None:
    """"""
    closed = nmpy.empty(image.shape[1:], dtype=nmpy.bool_)
    for dep in range(image.shape[0]):
        mp_.binary_closing(image[dep, ...], selem, out=closed)
        mp_.binary_opening(closed, selem, out=image[dep, ...])
