from __future__ import annotations

import tkinter as tknt
from typing import Any, Callable, Optional, Sequence, Tuple, Union

import numpy as nmpy

from brick.io.screen.type.tk_and_pil import (
    ColoredVersion,
    LabelColors,
    ScaledVersion,
    image_record_t,
)


array_t = nmpy.ndarray


class mip_widget_t(tknt.Label):
    """"""

    attributes = (
        "mip_records",
        "data_mode",  # "continuous", "discrete"
        "color_mode",  # "gray", "color"
        "gray_offset",
        "colormap",
        "colors",
        "static",
        "probe_able",
        "resizeable",
        "mip_axis_wgt",
        "selected_mip_axis",
        "probe_info_wgt",
        "companion_mip",
        "parent",
    )
    mip_records: Sequence[image_record_t]
    data_mode: str
    color_mode: str
    gray_offset: int
    colormap: str
    colors: Sequence[Union[float, array_t]]
    static: bool
    probe_able: bool
    resizeable: bool
    mip_axis_wgt: tknt.Menubutton
    selected_mip_axis: tknt.IntVar
    probe_info_wgt: Optional[tknt.Widget]
    companion_mip: Optional[mip_widget_t]
    parent: Union[tknt.Widget, tknt.Tk]

    def __init__(
        self,
        parent: Union[tknt.Widget, tknt.Tk],
        /,
    ):
        """"""
        super().__init__(parent, borderwidth=0, padx=0, pady=0)
        for attribute in mip_widget_t.attributes:
            setattr(self, attribute, None)

    @classmethod
    def NewForImage(
        cls,
        image: array_t,
        data_mode: str,
        color_mode: str,
        /,
        *,
        mip_axis: int = 0,  # Do not use negative values
        with_mip_selector: bool = False,
        should_pack_selector: bool = True,
        with_companion: mip_widget_t = None,
        gray_offset: int = 0,
        colormap: str = "viridis",
        static: bool = False,
        probe_able: bool = True,
        resizeable: bool = True,
        parent: Union[tknt.Widget, tknt.Tk] = None,
    ) -> mip_widget_t:
        """"""
        instance = cls(parent)

        # Do not comment out; Used in eval(attribute) below
        mip_records, colors = _AllMIPImages(
            image,
            data_mode,
            color_mode,
            gray_offset,
            colormap,
            probe_able,
            resizeable,
            parent,
        )

        if static and (with_mip_selector or (with_companion is not None)):
            raise ValueError(
                "MIP widget with MIP selector or companion cannot be static"
            )
        if with_mip_selector and (with_companion is not None):
            raise ValueError(
                "MIP widget cannot have both a MIP selector and a companion"
            )
        elif with_mip_selector:
            if should_pack_selector:
                selector_parent = instance
            else:
                selector_parent = parent
            # Do not comment out; Used in eval(attribute)
            mip_axis_wgt, selected_mip_axis = _MIPAxisSelectorWidget(
                mip_axis,
                instance.Update,
                image.shape,
                selector_parent,
            )
        elif with_companion is not None:
            selected_mip_axis = with_companion.selected_mip_axis
            _SetMIPAxisSelectorActions(
                selected_mip_axis,
                (instance.Update, with_companion.Update),
            )
        else:
            raise ValueError(
                "MIP widget must have either a MIP selector or a companion"
            )

        if with_mip_selector and should_pack_selector:
            raise NotImplementedError("Packing MIP selector not currently implemented")

        if resizeable:
            # Note: Binding cannot be done before super init
            instance.bind("<Configure>", instance._OnResize)

        # --- Saving required variables as object attributes
        for attribute in cls.attributes:
            try:
                value = eval(attribute)
            except NameError:
                value = None
            if value is not None:
                setattr(instance, attribute, value)

        instance.Update()

        return instance

    def UpdateImage(self, image: array_t, /) -> None:
        """"""
        if self.static:
            raise RuntimeError("Requesting the update of a static mip")

        self.mip_records, self.colors = _AllMIPImages(
            image,
            self.data_mode,
            self.color_mode,
            self.gray_offset,
            self.colormap,
            self.probe_able,
            self.resizeable,
            self.parent,
        )

        self.Update()

    def Update(self, *_, **__) -> None:
        """"""
        if self.selected_mip_axis is None:
            return

        mip_record = self.mip_records[self.selected_mip_axis.get()]
        self.configure(image=mip_record.tk_version)

    def AddProbe(
        self, probe_info_wgt: tknt.Widget, /, *, for_event: str = "<Motion>"
    ) -> None:
        """
        probe_info_wgt: Must have a text attribute updatable through the configure(text=...) method.

        The widget constructor could have been declared with a probe info widget as a parameter. However, to avoid
        requiring the probe info widget being created before the MIP widget, this 2-step option was chosen.
        """
        if not self.probe_able:
            raise ValueError(
                'Adding a probe to a MIP widget instantiated with "probe_able" to False'
            )

        self.probe_info_wgt = probe_info_wgt
        self.bind(for_event, self._DisplayInfo)

    def _DisplayInfo(self, event: tknt.EventType.Motion, /) -> None:
        """"""
        value, row, col = self.ValueAndIndicesUnderPointer(event.y, event.x)
        self.probe_info_wgt.configure(text=f"Label: {value} @ R={row}xC={col}")

    def _OnResize(self, event: tknt.EventType.Configure, /) -> None:
        """"""
        mip_record = self.mip_records[self.selected_mip_axis.get()]
        mip_record.Resize(event.width, event.height)
        self.configure(image=mip_record.tk_version)

    def ValueAndIndicesUnderPointer(
        self, pointer_row: int, pointer_col: int, /
    ) -> Tuple[Union[int, float], int, int]:
        """"""
        mip_record = self.mip_records[self.selected_mip_axis.get()]
        np_version = mip_record.np_version

        shape = np_version.shape
        row = int(round(shape[0] * pointer_row / self.winfo_height()))
        col = int(round(shape[1] * pointer_col / self.winfo_width()))

        try:
            value = np_version[row, col]
        except IndexError:
            # If margins/paddings/borders are all set to zero, this should not happen
            value = 0

        return value, row, col


def _MIPAxisSelectorWidget(
    current_axis: int,
    Action: Union[Callable, Sequence[Callable]],
    shape: Sequence[int],
    parent: Union[tknt.Widget, tknt.Tk],
    /,
) -> Tuple[tknt.Menubutton, tknt.IntVar]:
    """"""
    title = f"MIP Axis [{','.join(str(_lgt) for _lgt in shape)}]"
    selector = tknt.Menubutton(parent, text=title, relief="raised")

    selected_value = tknt.IntVar()
    selected_value.set(current_axis)

    menu = tknt.Menu(selector, tearoff=False)
    entries = ("First dim", "Second dim", "Third dim")
    for idx, entry in enumerate(entries):
        menu.add_radiobutton(label=entry, value=idx, variable=selected_value)
    menu.invoke(selected_value.get())

    # Set action only after calling invoke to avoid redundant call at window creation
    _SetMIPAxisSelectorActions(selected_value, Action)

    selector["menu"] = menu

    return selector, selected_value


def _SetMIPAxisSelectorActions(
    mip_selector_variable: tknt.IntVar,
    Action: Union[Callable, Sequence[Callable]],
) -> None:
    """"""
    if isinstance(Action, Callable):
        Actions = (Action,)
    else:
        Actions = Action

    def Callback(prm1: str, prm2: str, prm3: str, /) -> Any:
        #
        for OneAction in Actions:
            OneAction(prm1, prm2, prm3)

    mip_selector_variable.trace_add("write", Callback)


def _AllMIPImages(
    image: array_t,
    data_mode: str,
    color_mode: str,
    gray_offset: int,
    colormap: str,
    probe_able: bool,
    resizeable: bool,
    parent: Union[tknt.Widget, tknt.Tk],
    /,
) -> Tuple[Sequence[image_record_t], Sequence[Union[float, array_t]]]:
    """"""
    if data_mode == "continuous":
        colors = None
    else:
        colors = LabelColors(
            image, color_mode, gray_offset=gray_offset, colormap=colormap
        )

    mip_records = [
        _MIPImages(
            image,
            _axs,
            colors,
            parent,
        )
        for _axs in range(3)
    ]
    for record in mip_records:
        record.Optimize(probe_able, resizeable)

    return mip_records, colors


def _MIPImages(
    image: array_t,
    mip_axis: int,
    colors: Optional[Sequence[Union[float, array_t]]],
    parent: Union[tknt.Widget, tknt.Tk],
    /,
) -> image_record_t:
    """"""
    mip = nmpy.amax(image, axis=mip_axis)

    if colors is None:
        min_value = nmpy.amin(mip).item()
        max_value = nmpy.amax(mip).item()
        display_version = (255.0 / (max_value - min_value)) * (mip - min_value)
        display_version = nmpy.around(display_version).astype(nmpy.uint8)
    elif isinstance(colors[0], float):
        display_version = ScaledVersion(mip, colors)
    else:
        display_version = ColoredVersion(mip, colors)

    output = image_record_t(mip, display_version, parent)

    return output
