from __future__ import annotations

import tkinter as tknt
from typing import Any, Callable, Dict, List, Optional, Sequence, Tuple, Union

import numpy as nmpy
from matplotlib import pyplot as pypl
from matplotlib import ticker as tckr
from matplotlib.backends._backend_tk import NavigationToolbar2Tk as toolbar_widget_t
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg as matplotlib_widget_t
from matplotlib.figure import Figure as figure_t
from mpl_toolkits.mplot3d import Axes3D as axes_3d_t
from skimage import measure as sims

from brick.io.screen.type.three_d_image import image_record_t


array_t = nmpy.ndarray
isolevels_t = Sequence[float]


STANDARD_VIEW_POINTS = (
    {"elev": 10.0, "azim": 180.0},
    {"elev": 10.0, "azim": -90.0},
    {"elev": -90.0, "azim": -90.0},
)
DEFAULT_LABEL_STYLE = dict(boxstyle="round,pad=0.1", ec="k", lw=1)


class three_d_widget_t(tknt.Frame):
    """"""

    attributes = (
        "image_record",
        "isolevels",
        "lower_isovalue_margin",
        "upper_isovalue_margin",
        "labels",
        "colors",
        "figure",
        "axes",
        "isolines",
        "isosurfaces",
        "linked_labels",
        "annotation",
        "companion_wgt",
        "live_synchronization",
        "motion_event_id",
        "button_release_id",
        "plot_wgt",
        "toolbar",
        "isolevel_wgt",
        "selected_isolevel",
        "smoothing_width_wgt",
        "selected_smoothing_width",
    )
    image_record: image_record_t
    isolevels: isolevels_t
    lower_isovalue_margin: float
    upper_isovalue_margin: float
    labels: Tuple[int, ...]
    colors: Sequence[array_t]
    figure: pypl.Figure
    axes: pypl.Axes
    isolines: Sequence[Any]
    isosurfaces: List[Any]  # pypl.Axes.art3d.Poly3DCollection
    linked_labels: List[Tuple[pypl.Axes.text, pypl.Axes.line3D]]
    annotation: pypl.Axes.text
    companion_wgt: three_d_widget_t
    live_synchronization: bool
    motion_event_id: int
    button_release_id: int
    plot_wgt: matplotlib_widget_t
    toolbar: toolbar_widget_t
    isolevel_wgt: tknt.Scale
    selected_isolevel: tknt.DoubleVar
    smoothing_width_wgt: tknt.Scale
    selected_smoothing_width: tknt.DoubleVar

    defaults = {
        "isolines": [],
        "isosurfaces": [],
        "linked_labels": [],
        "live_synchronization": False,
    }

    def __init__(
        self,
        parent: Union[tknt.Widget, tknt.Tk],
        /,
        *args,
        **kwargs,
    ):
        """"""
        super().__init__(
            parent,
            *args,
            borderwidth=0,
            padx=0,
            pady=0,
            **kwargs,
            class_=self.__class__.__name__,
        )
        for attribute in three_d_widget_t.attributes:
            if attribute in three_d_widget_t.defaults:
                setattr(self, attribute, three_d_widget_t.defaults[attribute])
            else:
                setattr(self, attribute, None)

    @classmethod
    def NewForImage(
        cls,
        image: array_t,
        mode: str,
        colors: Optional[Sequence[Union[float, array_t]]],
        parent: Union[tknt.Widget, tknt.Tk],
        /,
        *args,
        mip_axis: int = 0,
        should_pack_slider: bool = True,
        initial_relative_isovalue: float = 0.5,
        lower_isovalue_margin: float = 0.1,
        upper_isovalue_margin: float = 0.1,
        **kwargs,
    ) -> three_d_widget_t:
        """"""
        instance = cls(parent, *args, **kwargs)

        image_record = image_record_t(image, mode)
        # Do not comment out; They are used in eval(attribute) below
        if mode == "continuous":
            colors = None
        else:
            isolevels, labels = _DecreasingIsolevelsAndLabels(image)

        figure, axes = _FigureAndAxes(image_record.resized.shape, image.shape, mip_axis)
        figure.canvas.mpl_connect("key_press_event", instance._OnKeyPress)
        figure.canvas.mpl_connect("scroll_event", instance._OnWheelScroll)
        plot_wgt = matplotlib_widget_t(figure, master=instance)
        toolbar = toolbar_widget_t(plot_wgt, instance, pack_toolbar=False)
        plot_wgt.draw()
        toolbar.update()

        if mode == "continuous":
            if should_pack_slider:
                slider_parent = instance
            else:
                slider_parent = parent
            isolevel_wgt, selected_isolevel = _IsolevelSelectorWidget(
                image,
                initial_relative_isovalue,
                lower_isovalue_margin,
                upper_isovalue_margin,
                instance.Update,
                slider_parent,
            )
            (
                smoothing_width_wgt,
                selected_smoothing_width,
            ) = _SmoothingWidthSelectorWidget(
                image_record.smoothing_width,
                instance.UpdateSmoothing,
                slider_parent,
            )
        else:
            isolevel_wgt = smoothing_width_wgt = None  # selected_smoothing_width

        plot_wgt.get_tk_widget().pack(fill=tknt.BOTH, expand=True)
        toolbar.pack(side=tknt.BOTTOM, fill=tknt.X)
        if (mode == "continuous") and should_pack_slider:
            isolevel_wgt.pack(side=tknt.BOTTOM, fill=tknt.X)
            smoothing_width_wgt.pack(side=tknt.BOTTOM, fill=tknt.X)

        # --- Saving required variables as object attributes
        for attribute in cls.attributes:
            try:
                value = eval(attribute)
            except NameError:
                value = None
            if value is not None:
                setattr(instance, attribute, value)

        instance.Update()

        return instance

    def UpdateImage(
        self,
        image: array_t,
        /,
        *,
        colors: Sequence[Union[float, array_t]] = None,
    ) -> None:
        """"""
        self.image_record.Update(image, self.selected_smoothing_width.get())
        self.colors = colors

        if self.isolevel_wgt is not None:
            min_bound, max_bound, level_extent = _IsolevelBounds(
                image, self.lower_isovalue_margin, self.upper_isovalue_margin
            )
            self.isolevel_wgt.configure(
                from_=min_bound,
                to=max_bound,
                resolution=level_extent / 100.0,
                tickinterval=level_extent / 10.0,
            )
            isolevel = self.selected_isolevel.get()
            isolevel = max(min(isolevel, max_bound), min_bound)
            self.isolevel_wgt.set(isolevel)
        else:
            self.isolevels, self.labels = _DecreasingIsolevelsAndLabels(
                self.image_record.original
            )

        self.Update()

    def UpdateSmoothing(self, *_, **__) -> None:
        """"""
        self.image_record.SetSmoothed(self.selected_smoothing_width.get())
        self.Update()

    def Update(self, *_, **__) -> None:
        """"""
        if self.image_record.mode == "discrete":
            self._ClearMIPProjection()
            self._PlotMIPProjections()

        self._ClearIsosurfaces()
        self._PlotIsosurfaces()

        self.figure.canvas.draw_idle()

    def _ClearMIPProjection(self) -> None:
        """"""
        for isolines_per_pane in self.isolines:
            for line in isolines_per_pane:
                line.remove()
        self.isolines = []

    def _PlotMIPProjections(self) -> None:
        """"""
        isolines = []

        mins, maxs, highs = _get_coord_info(self.axes)
        for axis, (limit_min, limit_max, high) in enumerate(zip(mins, maxs, highs)):
            isolines_per_pane = []

            projection = nmpy.amax(self.image_record.smoothed, axis=axis)
            if axis == 1:
                first_2d_dim = 1
            else:
                first_2d_dim = 0
            if high:
                # self.image_for_isosurface.shape[axis] does not account for margin
                coordinate = limit_max
            else:
                # 0 does not account for margin
                coordinate = limit_min
            for level, color in zip(self.isolevels, reversed(self.colors)):
                contours = sims.find_contours(projection, level)
                projection[projection >= level] = 0
                for contour in contours:
                    polygons = (
                        nmpy.full(contour.shape[0], coordinate),
                        contour[:, first_2d_dim],
                        contour[:, (first_2d_dim + 1) % 2],
                    )
                    polygons = polygons[(-axis):] + polygons[:(-axis)]
                    lines = self.axes.plot(*polygons, linewidth=1, color=color)
                    isolines_per_pane.extend(lines)

            isolines.append(isolines_per_pane)

        self.isolines = isolines

    def _AdjustMIPProjection(self) -> None:
        """"""
        mins, maxs, highs = _get_coord_info(self.axes)
        for axis, (limit_min, limit_max, high) in enumerate(zip(mins, maxs, highs)):
            if high:
                # self.image_for_isosurface.shape[axis] does not account for margin
                expected_coordinate = limit_max
            else:
                # 0 does not account for margin
                expected_coordinate = limit_min
            isolines_per_pane = self.isolines[axis]
            points = isolines_per_pane[0].get_data_3d()
            if points[axis][0] != expected_coordinate:
                for line in isolines_per_pane:
                    points = line.get_data_3d()
                    n_points = points[0].__len__()
                    points = (
                        points[:axis]
                        + (n_points * (expected_coordinate,),)
                        + points[(axis + 1) :]
                    )
                    line.set_data_3d(points)

    def _ClearIsosurfaces(self) -> None:
        """"""
        for isosurface in self.isosurfaces:
            isosurface.remove()
        self.isosurfaces = []

        self._ClearLabels()
        self._ClearAnnotations()

    def _PlotIsosurfaces(self) -> None:
        """"""
        if self.image_record.mode == "continuous":
            isolevels = (self.isolevel_wgt.get(),)
            labels = (1,)
            colors = (nmpy.array((0.0, 0.8, 0.0)),)
        else:
            isolevels = self.isolevels
            labels = self.labels
            colors = reversed(self.colors)

        image = self.image_record.smoothed.copy()
        n_triangles = 0
        bbox_style = DEFAULT_LABEL_STYLE.copy()
        for label, level, color in zip(labels, isolevels, colors):
            image_min = nmpy.amin(image).item()
            image_max = nmpy.amax(image).item()
            if (level < image_min) or (level > image_max):
                print(
                    f"{level}: Isolevel outside image range [{image_min},{image_max}]; Discarding"
                )
                continue

            try:
                vertices, triangles, *_ = sims.marching_cubes(
                    image, level=level, step_size=4
                )
            except RuntimeError as exception:
                print(exception.args[0] + " Discarding")
                continue

            n_triangles += triangles.shape[0]
            image[image >= level] = 0

            isosurface = self.axes.plot_trisurf(
                vertices[:, 0],
                vertices[:, 1],
                triangles,
                vertices[:, 2],
                color=color,
                lw=1,
            )
            self.isosurfaces.append(isosurface)

            self._PlotLabel(label, color, bbox_style, vertices)

        self._PlotAnnotations(n_triangles)

    def _ClearLabels(self) -> None:
        """"""
        for label, link in self.linked_labels:
            label.remove()
            link.remove()
        self.linked_labels = []

    def _PlotLabel(
        self,
        label: int,
        color: array_t,
        bbox_style: Dict[str, Any],
        vertices: array_t,
        /,
    ) -> None:
        """"""
        if self.image_record.mode == "discrete":
            center = nmpy.mean(vertices, axis=0, keepdims=True)
            rays = vertices - center
            radii = nmpy.linalg.norm(rays, axis=1)
            farthest = nmpy.argmax(radii)
            source = vertices[farthest, :]
            target = (
                source
                + (0.035 * min(self.image_record.resized.shape) / radii[farthest])
                * rays[farthest, :]
            )

            if nmpy.mean(color) >= 0.5:
                bk_color = 0.5 * color
            else:
                bk_color = nmpy.fmin(3.0 * color, 3 * (1.0,))
            bbox_style["fc"] = bk_color
            label = self.axes.text(
                *target,
                label,
                fontsize="small",
                fontweight="bold",
                color=color,
                bbox=bbox_style,
            )
            link = self.axes.plot(*zip(source, target), color=color)[0]

            self.linked_labels.append((label, link))

    def _ToggleLabels(self) -> None:
        """"""
        for label, link in self.linked_labels:
            label.set_visible(not label.get_visible())
            link.set_visible(not link.get_visible())

    def _ClearAnnotations(self) -> None:
        """"""
        if self.annotation is not None:
            self.annotation.remove()
            self.annotation = None

    def _PlotAnnotations(self, n_triangles: int, /) -> None:
        """"""
        zoom_factors = (f"{_zmf:.2f}" for _zmf in self.image_record.zoom_factors)
        self.annotation = self.axes.text2D(
            0.0,
            1.0,
            f"Zoom: {' '.join(zoom_factors)}\n" f"Triangles: {n_triangles}",
            fontsize="small",
            transform=self.axes.transAxes,
            color=3 * (0.75,),
        )

    def AddCompanionWidget(
        self, widget: three_d_widget_t, /, *, live_synchronization: bool = False
    ) -> None:
        """"""
        self.companion_wgt = widget
        self.live_synchronization = live_synchronization
        _ = self.figure.canvas.mpl_connect("button_press_event", self._OnButtonPress)

    def _SynchronizeCompanionView(self) -> None:
        """"""
        source = self.axes
        companion = self.companion_wgt
        target = companion.axes
        UpdateCompanion = companion.figure.canvas.draw_idle

        target.view_init(elev=source.elev, azim=source.azim)
        target.dist = source.dist

        if companion.isolines.__len__() > 0:
            companion._AdjustMIPProjection()

        UpdateCompanion()

    def _OnKeyPress(self, event, /) -> None:
        """"""
        elevation = self.axes.elev
        azimuth = self.axes.azim
        distance = self.axes.dist

        if event.key == "up":
            elevation -= 5.0
        elif event.key == "down":
            elevation += 5.0
        elif event.key == "left":
            azimuth += 5.0
        elif event.key == "right":
            azimuth -= 5.0
        elif event.key == "+":
            distance -= 1.0
        elif event.key == "-":
            distance += 1.0
        elif event.key == "l":
            self._ToggleLabels()
        else:
            return

        self.axes.view_init(elev=elevation, azim=azimuth)
        self.axes.dist = distance
        self._ApplyMotion(True)

    def _OnButtonPress(self, _, /) -> None:
        """"""
        self.motion_event_id = self.figure.canvas.mpl_connect(
            "motion_notify_event", self._OnMotion
        )
        self.button_release_id = self.figure.canvas.mpl_connect(
            "button_release_event", self._OnButtonRelease
        )

    def _OnButtonRelease(self, _, /) -> None:
        """"""
        self.figure.canvas.mpl_disconnect(self.motion_event_id)
        self.figure.canvas.mpl_disconnect(self.button_release_id)

        if not self.live_synchronization:
            self._SynchronizeCompanionView()

    def _OnMotion(self, event, /) -> None:
        """"""
        if event.inaxes == self.axes:
            self._ApplyMotion(self.live_synchronization)

    def _OnWheelScroll(self, event, /) -> None:
        """"""
        self.axes.dist += event.step
        self._ApplyMotion(True)

    def _ApplyMotion(self, live_synchronization: bool) -> None:
        """"""
        if self.isolines.__len__() > 0:
            self._AdjustMIPProjection()
            self.figure.canvas.draw_idle()

        if live_synchronization:
            self._SynchronizeCompanionView()


def _IsolevelSelectorWidget(
    image: array_t,
    initial_relative_isovalue: float,
    lower_isovalue_margin: float,
    upper_isovalue_margin: float,
    Action: Callable,
    parent: Union[tknt.Widget, tknt.Tk],
) -> Tuple[tknt.Scale, tknt.DoubleVar]:
    """"""
    min_bound, max_bound, level_extent = _IsolevelBounds(
        image, lower_isovalue_margin, upper_isovalue_margin
    )
    image_max = nmpy.amax(image).item()

    selected_value = tknt.DoubleVar()
    selected_value.set(initial_relative_isovalue * image_max)

    selector = tknt.Scale(
        parent,
        orient="horizontal",
        from_=min_bound,
        to=max_bound,
        resolution=level_extent / 100.0,
        tickinterval=level_extent / 10.0,
        variable=selected_value,
        label="Isovalue",
        takefocus=0,
    )

    # selected_value.trace_add("write", Action)
    selector.bind("<ButtonRelease-1>", Action)

    return selector, selected_value


def _IsolevelBounds(
    image: array_t,
    lower_isovalue_margin: float,
    upper_isovalue_margin: float,
) -> Sequence[float]:
    """"""
    image_min = nmpy.amin(image).item()
    image_max = nmpy.amax(image).item()
    image_extent = image_max - image_min
    level_extent = (1.0 - lower_isovalue_margin - upper_isovalue_margin) * image_extent

    min_bound = image_min + lower_isovalue_margin * image_extent
    max_bound = image_max - upper_isovalue_margin * image_extent

    return min_bound, max_bound, level_extent


def _SmoothingWidthSelectorWidget(
    initial_smoothing_width: float,
    Action: Callable,
    parent: Union[tknt.Widget, tknt.Tk],
) -> Tuple[tknt.Scale, tknt.DoubleVar]:
    """"""
    selected_value = tknt.DoubleVar()
    selected_value.set(initial_smoothing_width)

    selector = tknt.Scale(
        parent,
        orient="horizontal",
        from_=0.0,
        to=5.0,
        resolution=5.0 / 100.0,
        tickinterval=5.0 / 10.0,
        variable=selected_value,
        label="Smoothing Width",
        takefocus=0,
    )

    # selected_value.trace_add("write", Action)
    selector.bind("<ButtonRelease-1>", Action)

    return selector, selected_value


def _DecreasingIsolevelsAndLabels(
    image: array_t,
) -> Tuple[isolevels_t, Tuple[int, ...]]:
    """"""
    labels = nmpy.unique(image)

    levels = labels
    levels = 0.5 * (levels[:-1] + levels[1:])
    levels = nmpy.flip(levels)

    return levels, tuple(reversed(labels.astype(nmpy.uint64, copy=False)))


def _FigureAndAxes(
    shape: Tuple[int, ...], actual_shape: Tuple[int, ...], mip_axis: int
) -> Tuple[pypl.Figure, pypl.Axes]:
    """"""
    figure = figure_t()
    axes = figure.add_subplot(111, projection=axes_3d_t.name)
    # (("left", "right"), ("bottom", "top"), ("bottom", "top"))
    for axis, limit_p_1, actual_limit in zip(("x", "y", "z"), shape, actual_shape):
        SetLimits = getattr(axes, f"set_{axis}lim3d")
        arguments = {f"{axis}max": 0, f"{axis}min": limit_p_1}
        SetLimits(**arguments)

        mtpl_axis = getattr(axes, f"{axis}axis")
        formatter = lambda _vle, _pos: str(int(round(actual_limit * _vle / limit_p_1)))
        formatter = tckr.FuncFormatter(formatter)
        mtpl_axis.set_major_formatter(formatter)
    axes.invert_xaxis()
    axes.invert_zaxis()
    axes.set_box_aspect(shape)
    axes.view_init(**STANDARD_VIEW_POINTS[mip_axis])
    axes.set_xlabel("First")
    axes.set_ylabel("Second")
    axes.set_zlabel("Third")

    return figure, axes


_PLANES = (
    (0, 3, 7, 4),
    (1, 2, 6, 5),  # yz planes
    (0, 1, 5, 4),
    (3, 2, 6, 7),  # xz planes
    (0, 1, 2, 3),
    (4, 5, 6, 7),  # xy planes
)


def _get_coord_info(axes: pypl.Axes) -> Tuple[array_t, array_t, array_t]:
    """
    https://github.com/matplotlib/matplotlib/blob/master/lib/mpl_toolkits/mplot3d/axis3d.py
    """
    mins, maxs = nmpy.array(
        [
            axes.get_xbound(),
            axes.get_ybound(),
            axes.get_zbound(),
        ]
    ).T
    # centers = (maxs + mins) / 2.0
    deltas = (maxs - mins) / 12.0
    mins = mins - deltas / 4.0
    maxs = maxs + deltas / 4.0

    vals = mins[0], maxs[0], mins[1], maxs[1], mins[2], maxs[2]
    tc = axes.tunit_cube(vals, axes.M)
    avgz = [tc[p1][2] + tc[p2][2] + tc[p3][2] + tc[p4][2] for p1, p2, p3, p4 in _PLANES]
    highs = nmpy.array([avgz[2 * i] < avgz[2 * i + 1] for i in range(3)])

    return mins, maxs, highs


# coords = tuple(tuple(range(_lgt)) for _lgt in shape)
# new_length = int(round(MIN_LENGTH_RATIO * max_length))
# new_coords = coords[:smallest_axis] + (tuple(range(new_length)),) + coords[(smallest_axis+1):]
#
# image = ntpl.interpn(coords, image, point)
