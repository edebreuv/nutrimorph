# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import sys as sy_

import matplotlib as mpl_
import matplotlib.pyplot as pl_
import skimage.io as io_
from PySide6.QtWidgets import *

from brick.type.base import array_t


mpl_.use("TkAgg")

CHANNEL = "G"
DATA_PATH = "././data/DIO_6H_6_1.70bis_2.2_3.tif"
IMAGE = io_.imread(DATA_PATH)
# image = image[:,:,:,1]


class image_verification(QWidget):
    def __init__(self, image: array_t = None, channel: str = None, parent=None):
        super(image_verification, self).__init__(parent)
        self.channel = channel
        self.image = image
        # Create widgets
        # --Text
        self.text0 = QLabel(parent=None, text="The image input must be not constant.")
        self.text1 = QLabel(parent=None, text="The image has only one color channel.")
        self.text2 = QLabel(
            parent=None,
            text=f"The image has only 3 dimensions. However, a value for the 'channel' parameter"
            f"is specified: {channel}. Continue with this image?",
        )
        self.text3 = QLabel(
            parent=None,
            text=f"The image has multiple color channels. The channel {channel} is specified"
            f" in the parameters. Continue with the resulting image?",
        )
        self.text4 = QLabel(
            parent=None,
            text="The image has multiple color channels. Error in the value of the parameter channel.",
        )
        self.text5 = QLabel(
            parent=None,
            text=f"The image dimensions are not correct: {image.ndim}, instead of 3 or 4.",
        )
        # --Buttons
        self.button_quit = QPushButton("Quit and modify the parameters.")
        self.button_continue = QPushButton("Continue.")
        # Button signal
        self.button_continue.clicked.connect(self.Continue)
        self.button_quit.clicked.connect(self.Quit)

        if image is not None:
            self.ImVerif()
            # self.ReturnImage()

            # self.PlotImage(image)

    def Continue(self):
        self.close()

    @staticmethod
    def Quit():
        sy_.exit(0)

    def ReturnImage(self):
        return self.image

    # def PlotImage(self, image: array_t) -> None :
    #     im = image.copy()
    #     im = nmpy.rint(im).astype(nmpy.uint8)
    #     im = QImage(im, im.shape[1], im.shape[0], im.shape[1] * 3, QImage.Format_RGB888)
    #     pix = QPixmap.fromImage(im)
    #     img = QLabel(self)
    #     img.setPixmap(pix)

    def ImVerif(self) -> array_t:
        # The image must not be constant
        if self.image.max() == self.image.min():
            value_error = "The image input must be not constant."
            print(value_error)

            layout = QVBoxLayout()
            layout.addWidget(self.text0)
            layout.addWidget(self.button_quit)
            self.setLayout(layout)

        # Verification of the dimension of the image and its coherence with the parameters (channel)
        elif self.image.ndim == 3:
            print("The image has only one color channel.")
            pl_.imshow(self.image[10, :, :], cmap="gray")
            pl_.show(block=True)

            text = self.text1

            if CHANNEL is not None:
                value_error = (
                    f'The image has only 3 dimensions. However, a value for the "channel" parameter is '
                    f"specified : {CHANNEL}. Give the channel the value None? "
                )
                print(value_error)
                text = self.text2

            layout = QVBoxLayout()
            layout.addWidget(text)
            layout.addWidget(self.button_continue)
            if text == self.text2:
                layout.addWidget(self.button_quit)
            self.setLayout(layout)

        elif self.image.ndim == 4:
            if CHANNEL == "R" or CHANNEL == "G" or CHANNEL == "B":
                # not changed into if --channel in 'RGB'-- because if channel='RG' => True.
                value_error = (
                    f"The image has multiple color channels. The channel {CHANNEL} is specified in the "
                    f"parameters. "
                )
                print(value_error)
                text = self.text3

                self.image = self.image[:, :, :, "RGB".find(CHANNEL)]
                pl_.imshow(self.image[10, :, :], cmap="gray")
                pl_.show(block=True)

                # The obtained image must not be constant
                if self.image.max() == self.image.min():
                    value_error = "The image input must be not constant."
                    print(value_error)
                    text = self.text0

                layout = QVBoxLayout()
                layout.addWidget(text)
                if text == self.text3:
                    layout.addWidget(self.button_continue)
                layout.addWidget(self.button_quit)
                self.setLayout(layout)

                return self.image  # TODO: How to return the image ??

            else:
                print(
                    "The image has multiple color channels. Error in the value of the parameter channel."
                )

                layout = QVBoxLayout()
                layout.addWidget(self.text4)
                layout.addWidget(self.button_quit)
                self.setLayout(layout)

        elif self.image.ndim != 4 and self.image.ndim != 3:
            print(
                f"The image dimensions are not correct: {self.image.ndim}, instead of 3 or 4."
            )

            layout = QVBoxLayout()
            layout.addWidget(self.text5)
            layout.addWidget(self.button_quit)
            self.setLayout(layout)


if __name__ == "__main__":
    app = QApplication()
    verif = image_verification(IMAGE, CHANNEL)
    verif.show()
    sy_.exit(app.exec_())
