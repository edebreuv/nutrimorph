import tkinter as tknt
from typing import Optional, Sequence, Tuple, Union

import numpy as nmpy
from matplotlib import cm as clmp
from PIL import Image as plim
from PIL import ImageTk as pltk


array_t = nmpy.ndarray
pil_image_t = plim.Image
tk_image_t = pltk.PhotoImage


class image_record_t:
    """"""

    __slots__ = (
        "np_version",
        "pil_version_original",
        "pil_version",
        "tk_version",
        "parent",
    )
    attributes = __slots__

    np_version: Optional[array_t]
    pil_version_original: Optional[pil_image_t]
    pil_version: pil_image_t
    tk_version: tk_image_t
    parent: Union[tknt.Widget, tknt.Tk]

    def __init__(
        self,
        np_version: array_t,
        display_version: array_t,
        parent: Union[tknt.Widget, tknt.Tk],
        /,
    ):
        """"""
        # Do not comment out; They are used in eval(attribute) below
        tk_version, pil_version = TkAndPILImagesFromNumpyArray(display_version, parent)
        pil_version_original = pil_version

        # --- Saving required variables as object attributes
        for attribute in image_record_t.attributes:
            setattr(self, attribute, eval(attribute))

    def Optimize(
        self,
        probe_able: bool,
        resizeable: bool,
        /,
    ) -> None:
        """"""
        if not probe_able:
            self.np_version = None
        if not resizeable:
            self.pil_version_original = None

    def Resize(self, width: int, height: int, /) -> None:
        """"""
        self.pil_version = self.pil_version_original.resize((width, height))
        self.tk_version = pltk.PhotoImage(master=self.parent, image=self.pil_version)


def LabelColors(
    image: array_t, mode: str, /, *, gray_offset: int = 0, colormap: str = "viridis"
) -> Sequence[Union[float, array_t]]:
    """
    mode: "gray", "color"
    gray_offset: Value of darkest non-background intensity
    """
    # Keep max instead of unique.size to maintain colors in case of deletion
    n_labels = int(nmpy.amax(image))

    if mode == "gray":
        output = nmpy.linspace(max(gray_offset, 1) / 255.0, 1.0, num=n_labels)
    elif mode == "color":
        LinearValueToRGB = clmp.get_cmap(colormap)
        if n_labels > 1:
            output = tuple(
                nmpy.array(LinearValueToRGB((_lbl - 1.0) / (n_labels - 1.0))[:3])
                for _lbl in range(1, n_labels + 1)
            )
        else:
            output = (nmpy.array(LinearValueToRGB(1.0)[:3]),)
    else:
        raise ValueError(f'{mode}: Invalid mode; Expected="gray" or "color"')

    return output


def ScaledVersion(image: array_t, gray_levels: Sequence[float], /) -> array_t:
    """"""
    output = nmpy.zeros(image.shape, dtype=nmpy.uint8)

    for label, gray_level in enumerate(gray_levels, start=1):
        output[image == label] = round(255.0 * gray_level)

    return output


def ColoredVersion(image: array_t, colors: Sequence[array_t], /) -> array_t:
    """"""
    output = nmpy.zeros(image.shape + (3,), dtype=nmpy.uint8)

    for label, color in enumerate(colors, start=1):
        output[image == label] = nmpy.around(255.0 * color)

    return output


def TkAndPILImagesFromNumpyArray(
    array: array_t, parent: Union[tknt.Widget, tknt.Tk], /
) -> Tuple[tk_image_t, pil_image_t]:
    """"""
    pil_image = plim.fromarray(array)
    tk_image = pltk.PhotoImage(master=parent, image=pil_image)

    return tk_image, pil_image
