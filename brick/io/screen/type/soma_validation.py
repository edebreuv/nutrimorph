# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

import tkinter as tknt
from typing import Optional, Sequence

import numpy as nmpy
import skimage.segmentation as sisg

from brick.io.screen.type.mip_widget import mip_widget_t
from brick.io.screen.type.three_d_widget import three_d_widget_t


array_t = nmpy.ndarray


STATIC_ROW_MIN_HEIGHT = 30
INITIAL_RELATIVE_ISOVALUE = 0.6


class soma_validation_window_t:
    """"""

    __slots__ = (
        "gfp",
        "lmap",
        "main_window",
        "three_d_selector",
        "gfp_wgt",
        "gfp_3d_wgt",
        "lmap_wgt",
        "lmap_3d_wgt",
        "cursor_nfo",
        "invisible_widgets",
    )
    attributes = __slots__

    gfp: array_t
    lmap: array_t
    main_window: tknt.Tk
    three_d_selector: tknt.Checkbutton
    gfp_wgt: mip_widget_t
    gfp_3d_wgt: Optional[three_d_widget_t]
    lmap_wgt: mip_widget_t
    lmap_3d_wgt: Optional[three_d_widget_t]
    cursor_nfo: tknt.Label
    invisible_widgets: Sequence

    def __init__(
        self,
        gfp: array_t,
        lmap: array_t,
        /,
        *,
        mip_axis: int = -1,
        gray_offset: int = 0,
        colormap: str = "viridis",
    ):
        """
        with_cm: "plasma" and "viridis" seem to be good options
        """
        exec(f"{'='.join(soma_validation_window_t.attributes)} = None")

        main_window = tknt.Tk()

        # ---- Creation of widgets
        if mip_axis < 0:
            mip_axis = gfp.ndim + mip_axis
        gfp_wgt = mip_widget_t.NewForImage(
            gfp,
            "continuous",
            "gray",
            mip_axis=mip_axis,
            with_mip_selector=True,
            should_pack_selector=False,
            gray_offset=gray_offset,
            probe_able=False,
            parent=main_window,
        )
        lmap_wgt = mip_widget_t.NewForImage(
            lmap,
            "discrete",
            "color",
            mip_axis=mip_axis,
            with_companion=gfp_wgt,
            colormap=colormap,
            parent=main_window,
        )
        state_variable = tknt.IntVar()
        Toggle3D = lambda *args, **kwargs: self._Toggle3D(state_variable)
        three_d_selector = tknt.Checkbutton(
            main_window, text="3D View", variable=state_variable, command=Toggle3D
        )
        cursor_nfo = tknt.Label(main_window, text="")
        done_button = tknt.Button(main_window, text="Done", command=main_window.quit)

        # --- Event management
        lmap_wgt.AddProbe(cursor_nfo)
        lmap_wgt.bind("<Button-1>", self._DeleteSoma)

        # --- Widget placement in grid
        next_available_row = 0

        gfp_wgt.mip_axis_wgt.grid(row=next_available_row, column=0)
        three_d_selector.grid(row=next_available_row, column=1)
        next_available_row += 1

        # sticky=... solves the super-slow-to-resize issue!
        gfp_wgt.grid(
            row=next_available_row, column=0, sticky=tknt.W + tknt.E + tknt.N + tknt.S
        )
        lmap_wgt.grid(
            row=next_available_row, column=1, sticky=tknt.W + tknt.E + tknt.N + tknt.S
        )
        # Leave 2 rows free for isovalue slider and smoothing width slider
        next_available_row += 3

        cursor_nfo.grid(row=next_available_row, column=0)
        done_button.grid(row=next_available_row, column=1)
        next_available_row += 1

        # --- Window resize management
        n_grid_cols, n_grid_rows = main_window.grid_size()
        for row in range(n_grid_rows):
            if row == 1:
                main_window.rowconfigure(1, weight=10)
            else:
                main_window.rowconfigure(row, weight=1, minsize=STATIC_ROW_MIN_HEIGHT)
        for col in range(n_grid_cols):
            main_window.columnconfigure(col, weight=1)

        # --- Saving required variables as object attributes
        for attribute in soma_validation_window_t.attributes:
            setattr(self, attribute, eval(attribute))
        self.invisible_widgets = ()

    def LaunchValidation(self) -> tuple[array_t, int]:
        """"""
        self.main_window.mainloop()
        self.main_window.destroy()

        relabeled, *_ = sisg.relabel_sequential(self.lmap)

        return relabeled, nmpy.amax(relabeled).item()

    def _DeleteSoma(self, event: tknt.EventType.ButtonPress, /) -> None:
        """"""
        label, *_ = self.lmap_wgt.ValueAndIndicesUnderPointer(event.y, event.x)

        if label > 0:
            lmap = self.lmap
            where_label = lmap == label
            if nmpy.all(nmpy.logical_or(lmap == 0, where_label)):
                return
            lmap[where_label] = 0

            self.lmap_wgt.UpdateImage(lmap)
            if self.lmap_3d_wgt is not None:
                self.lmap_3d_wgt.UpdateImage(lmap, colors=self.lmap_wgt.colors)

    def _Toggle3D(self, state: tknt.IntVar, /) -> None:
        """"""
        three_d_state = state.get()

        if three_d_state == 0:
            soon_invisible_widgets = (
                self.gfp_3d_wgt,
                self.lmap_3d_wgt,
                self.gfp_3d_wgt.isolevel_wgt,
                self.gfp_3d_wgt.smoothing_width_wgt,
            )
        else:
            soon_invisible_widgets = (
                self.gfp_wgt,
                self.lmap_wgt,
                self.gfp_wgt.mip_axis_wgt,
            )
        for widget in soon_invisible_widgets:
            widget.grid_remove()

        if self.invisible_widgets.__len__() > 0:
            for widget in self.invisible_widgets:
                widget.grid()
        else:
            mip_axis = self.gfp_wgt.selected_mip_axis.get()
            gfp_3d_wgt = three_d_widget_t.NewForImage(
                self.gfp,
                "continuous",
                None,
                self.main_window,
                mip_axis=mip_axis,
                should_pack_slider=False,
                initial_relative_isovalue=INITIAL_RELATIVE_ISOVALUE,
            )
            lmap_3d_wgt = three_d_widget_t.NewForImage(
                self.lmap,
                "discrete",
                self.lmap_wgt.colors,
                self.main_window,
                mip_axis=mip_axis,
            )
            gfp_3d_wgt.AddCompanionWidget(lmap_3d_wgt)
            lmap_3d_wgt.AddCompanionWidget(gfp_3d_wgt)

            gfp_3d_wgt.grid(row=1, column=0, sticky=tknt.W + tknt.E + tknt.N + tknt.S)
            lmap_3d_wgt.grid(row=1, column=1, sticky=tknt.W + tknt.E + tknt.N + tknt.S)
            gfp_3d_wgt.isolevel_wgt.grid(
                row=2, column=0, sticky=tknt.W + tknt.E + tknt.N + tknt.S
            )
            gfp_3d_wgt.smoothing_width_wgt.grid(
                row=3, column=0, sticky=tknt.W + tknt.E + tknt.N + tknt.S
            )
            self.gfp_3d_wgt = gfp_3d_wgt
            self.lmap_3d_wgt = lmap_3d_wgt
        self.invisible_widgets = soon_invisible_widgets

        if three_d_state == 0:
            self.cursor_nfo.configure(text="")
        else:
            self.cursor_nfo.configure(text="No MIP info in 3-D mode")
