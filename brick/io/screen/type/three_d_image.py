from typing import Optional, Tuple

import numpy as nmpy
from scipy import ndimage as spim
from skimage import filters as fltr


array_t = nmpy.ndarray


class image_record_t:
    """"""

    __slots__ = (
        "original",
        "resized",
        "smoothed",
        "mode",  # "continuous", "discrete"
        "zoom_factors",
        "smoothing_width",
    )
    attributes = __slots__

    original: array_t
    resized: array_t
    smoothed: array_t
    mode: str
    zoom_factors: Tuple[float, float, float]
    smoothing_width: Optional[float]

    IMAGE_DTYPE = nmpy.float32
    MAX_LENGTH = 500
    MIN_LENGTH_RATIO = 0.25
    INITIAL_SMOOTHING_WIDTH = 1.0

    defaults = {}

    def __init__(
        self,
        image: array_t,
        mode: str,
        /,
    ):
        """"""
        exec(f"{'='.join(image_record_t.attributes)} = None")
        for attribute in image_record_t.attributes:
            if attribute in image_record_t.defaults:
                setattr(self, attribute, image_record_t.defaults[attribute])
            else:
                setattr(self, attribute, eval(attribute))

        self.Update(image, image_record_t.INITIAL_SMOOTHING_WIDTH)

    def Update(self, image: array_t, smoothing_width: float, /) -> None:
        """"""
        self.smoothing_width = None  # Ensures that new image will be smoothed even if smoothing width is the same
        self.original = _Retyped(image)
        self.SetResized()
        self.SetSmoothed(smoothing_width)

    def SetResized(self) -> None:
        """"""
        shape = self.original.shape
        min_length = min(shape)
        max_length = max(shape)
        zoom_factors = (1.0, 1.0, 1.0)

        if min_length < image_record_t.MIN_LENGTH_RATIO * max_length:
            smallest_axis = shape.index(min_length)
            zoom_factors = (
                zoom_factors[:smallest_axis]
                + (image_record_t.MIN_LENGTH_RATIO * max_length / min_length,)
                + zoom_factors[(smallest_axis + 1) :]
            )
        if max_length > image_record_t.MAX_LENGTH:
            scaling = image_record_t.MAX_LENGTH / max_length
            zoom_factors = tuple(scaling * _zmf for _zmf in zoom_factors)

        self.zoom_factors = zoom_factors
        if any(_zmf != 1.0 for _zmf in zoom_factors):
            if self.mode == "continuous":
                order = 3
            else:
                order = 0
            resized = spim.zoom(self.original, zoom_factors, order=order)
            self.resized = _Retyped(resized)
        else:
            self.resized = self.original

    def SetSmoothed(self, smoothing_width: float, /) -> None:
        """"""
        if self.mode == "continuous":
            if smoothing_width != self.smoothing_width:
                smoothed = fltr.gaussian(self.resized, smoothing_width)
                self.smoothed = _Retyped(smoothed)
                self.smoothing_width = smoothing_width
        else:
            self.smoothed = self.resized


def _Retyped(image: array_t) -> array_t:
    """"""
    return image.astype(image_record_t.IMAGE_DTYPE, copy=False)
