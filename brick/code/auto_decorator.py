import inspect as nspt
import pathlib
import sys as sstm
from types import FunctionType as function_t
from types import MethodType as method_t

import __main__ as main_package
from memory_profiler import profile as Profiled


def MemoryProfileCallables(module: str) -> None:
    """
    module: typically, __name__
    """
    module_name = module
    module = sstm.modules[module]
    functions = (
        (_nme, _elm)
        for _nme, _elm in nspt.getmembers(module)
        if (isinstance(_elm, (function_t, method_t)) and _elm.__module__ == module_name)
    )

    memory_profile_report = open(
        pathlib.Path(main_package.__file__).parent / "memory_profiler.txt", "w+"
    )
    profile = lambda _fct: Profiled(_fct, stream=memory_profile_report)

    for name, function in functions:
        setattr(module, name, profile(function))
